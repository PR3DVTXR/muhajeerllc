from django.shortcuts import render
from .models import Product

# Create your views here.
def home(request):
    return render(request, 'app1/home.html')

def categorie(request):
    product = Product.objects.all()
    return render(request, 'app1/categorie.html', {'product':product})