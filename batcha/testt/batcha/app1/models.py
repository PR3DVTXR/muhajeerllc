from django.db import models

# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=50)
    price = models.CharField(max_length=4)
    description = models.TextField()
    img1 = models.ImageField(upload_to='product/images/')
    img2 = models.ImageField(upload_to='product/images/')

    def __str__(self):
        return self.title
    
